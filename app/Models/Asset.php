<?php

namespace App\Models;

class Asset extends BaseModel
{
    protected $primaryKey = null;

    public $incrementing = false;

    public $readOnly = true;
}
