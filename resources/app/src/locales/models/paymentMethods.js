export default {
  fr: {
    fields: {
      credit_card_type: "type de compte",
      external_id: "numéro de compte",
      name: "nom associé au compte",
      type: "type",
      types: {
        credit_card: "carte de crédit",
        bank_account: "compte de banque",
      },
    },
    types: {
      credit_card: "carte de crédit",
      bank_account: "compte de banque",
    },
    model_name: "mode de paiement | modes de paiement",
  },
};
