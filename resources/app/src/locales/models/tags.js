export default {
  fr: {
    fields: {
      id: "ID",
      name: "nom",
      slug: "nom système",
    },
    list: {
      create: "ajouter un mot-clé",
      selected: "{count} mot-clé sélectionné | {count} mots-clés sélectionnés",
    },
    model_name: "mot-clé | mots-clés",
  },
};
