/* eslint-disable */
export default {
  fr: {
    title: "",
    home: [
      {
        title: "locomotion.app ne se télécharge pas!",
        content:
          "<p>Il n’y a pas d’application à télécharger sur l’AppStore, GooglePlay ou d’autres magasins d’app. locomotion.app est simplement un site Web et vous y êtes! Il fonctionne à l’aide d’un navigateur (Firefox, Chrome, Safari…) sur votre ordinateur ou votre téléphone intelligent. Vous pouvez ajouter un raccourci sur votre téléphone intelligent, ça aurait l’air d’une app!</p>",
      },
      {
        title: "Emprunter, ça coûte combien?",
        content:
          "<p>L’inscription à LocoMotion est gratuite et il n’y a pas d’abonnement.</p> <p>✓ Vous utilisez un vélo ou une remorque à vélo? C’est gratuit, profitez-en!</p> <p>Vous utilisez l’auto de votre voisin-e? Coût d’un emprunt = distance + durée + assurance.</p> <p>Le prix est comparable ou parfois moins cher que les services d’autopartage du Québec.</p>",
      },
      {
        title: "Partager mon auto, comment ça marche?",
        content:
          "<p>Une personne au compte LocoMotion validé (pièces d’identité et dossier de conduite validé) vous fera une demande. Vous pourrez ajuster les disponibilités de votre auto. Comme propriétaire d’une auto en partage, vous avez toujours le choix d’accepter ou non une demande de réservation. Aussi, en apprenant à connaître vos voisin-e-s, il est plus facile de se faire confiance. Vous recevrez une contribution financière de votre voisin-e. Et enfin, n’oubliez pas qu’il y a une assurance pour couvrir les trajets!</p>",
      },
      {
        title: "Partager mon auto, combien je reçois?",
        content:
          '<p>Recevez une contribution des personnes qui l’utilisent. La contribution vise à couvrir les frais réels et se base sur la distance parcourue et la durée. Il y a deux catégories d’auto, qui correspondent à des coûts différents.</p><p>Pour le détail des coûts, voir la <a href="http://bit.ly/locomotion-tarification" target="_blank">tarification LocoMotion</a>.</p>',
      },
      {
        title: "Quel âge faut-il pour participer?",
        content:
          "<p>Pour les vélos et les remorques à vélo, vous devez avoir 18 ans.</p><p>Pour le partage d’auto, vous devez avoir 21 ans et un permis de conduire québécois.</p>",
      },
      {
        title: "Ça fonctionne avec les permis de conduire étrangers?",
        content:
          "<p>Non, malheureusement. Pour conduire une auto en partage avec LocoMotion, vous avez besoin d’un permis québécois.</p>",
      },
      {
        title: "Pourquoi LocoMotion existe seulement dans certains quartiers?",
        content:
          "<p>LocoMotion n’est pas un service clé en main, c’est un projet citoyen en cours de développement. On le construit ensemble en se concentrant dans certains quartiers avant de l’adapter à d’autres réalités. Pour le moment, LocoMotion est disponible dans La Petite-Patrie et Ahuntsic.</p>",
      },
      {
        title: "Quels sont les critères pour partager mon auto?",
        content:
          "<p>Votre auto doit avoir moins de 15 ans et une valeur à neuf maximale de 50&nbsp;000&nbsp;$. Vous devrez aussi aviser votre assureur (lettre fournie). Pas besoin de prévenir votre assureur si c’est Desjardins ou La Personnelle.</p>",
      },
      {
        title: "Qu'est-ce qui arrive en cas d'accident?",
        content:
          "<p>Respirez. Si vous avez un accident avec une auto empruntée avec LocoMotion, on a inscrit la marche à suivre dans le carnet de bord de l’auto ou dans votre trajet en cours sur locomotion.app</p><p>En cas d’accident, notre couverture Desjardins Assurances (spécialement conçue pour le projet) prend en charge la responsabilité civile et les réparations.<p><ul><li>Couverture responsabilité civile à hauteur de 2&nbsp;M$</li><li>Franchise de 100&nbsp;$ pour les dommages causés notamment par le feu, le vol, le vandalisme, le bris de vitres.</li><li>Franchise de 500&nbsp;$ pour les dommages causés par une collision ou un versement</li>",
      },
      {
        title: "Y-a-t’il d’autres véhicules que des autos à partager?",
        content:
          '<p>En plus des voitures variées de vos voisins et voisines, vous aurez accès à une variété de moyens de transport&nbsp;: vélo-cargo, vélo électrique, remorque à attacher à votre vélo…  Vous pourrez le choisir selon votre besoin. LocoMotion c’est la cerise de votre <a href="https://equiterre.org/solution/cocktail-transport" target="_blank">cocktail transport</a>!</p>',
      },
    ],
    sections: [
      {
        title: "Je débute avec LocoMotion",
        count: "1",
        questions: [
          {
            title: "Comment bien démarrer?",
            content:
              '<p>Que vous vouliez prêter ou emprunter, toutes les infos pour bien commencer l’aventure se trouvent dans le <a href="http://bit.ly/locomotion-bienvenue" target="_blank">guide de départ</a>!</p>',
          },
        ],
      },
      {
        title: "Emprunter&nbsp;: auto, vélo, remorque à vélo",
        count: "1",
        questions: [
          {
            title: "Comment faire une réservation?",
            content:
              "<p>Il y a deux affichages possibles&nbsp;: carte ou liste.</p><p>Indiquez la période de réservation souhaitée puis cliquez sur Rechercher. Les véhicules disponibles affichent alors le bouton &laquo;&nbsp;Demande d’emprunt&nbsp;&raquo;.</p><p>Si le véhicule est collectif, c’est tout ce qu’il y a à faire, il est réservé pour vous!</p><p>Si le véhicule appartient à une personne, contactez-la! Texto, appel... Profitez-en pour faire connaissance. Si son véhicule est disponible, la personne acceptera votre demande sur locomotion.app. Vous pourrez alors pré-payer l’emprunt et poursuivre le processus sur locomotion.app jusqu’à la fin de l’emprunt.</p>",
          },
        ],
      },
      {
        title: "Remorques à vélo, attache-remorque et cadenas",
        count: "7",
        questions: [
          {
            title: "Les remorques sont disponibles durant quelle période?",
            content:
              "<p>Ça dépend de la météo! (Les abris à remorques à vélo ne doivent pas gêner le déneigement). Environ de avril à novembre.</p>",
          },
          {
            title: "Comment installer l’attache-remorque sur mon vélo?",
            content:
              '<p>Pour traîner la remorque, vous avez besoin d’une attache-remorque. Vous l’installez une fois sur votre vélo puis vous la laissez en place!</p><p>Vous pouvez prendre une attache-remorque par personne et c’est gratuit. Si vous avez plusieurs vélos, merci d’utiliser la même au besoin. Pour savoir où la trouver, rendez-vous sur le <a href="http://bit.ly/locomotion-bienvenue" target="_blank">Guide de départ</a>.</p><p>L’attache est compatible avec la majorité des vélos.</p><p>Pour installer l’attache-remorque&nbsp;:</p><img src="/FAQ-attache-remorque.png" alt=Exemple d\'attache-remorque installé" style="float: left; max-width: 12rem; margin-right: 1rem;"/><ol style="list-style: inside decimal;"><li>enlevez le boulon de la roue arrière du vélo;</li><li>placez la pièce de façon (presque) parallèle au sol;</li><li>remplacez le boulon et serrez bien.</li></ol><p>Lorsque vous n’utilisez pas la remorque, l’attache grisel reste dans la pièce fixe (généralement noire) sur votre vélo</p>',
          },
          {
            title: "Les cadenas Noke Pro : comment ça marche?",
            content:
              '<p>Vous devez activer votre compte Noke Pro grâce un courriel de Noke Pro reçu au moment de votre inscription à LocoMotion. Vous devez ensuite installer l’application sur votre cellulaire.</p><p>Vous pourrez alors débarrer les cadenas avec votre téléphone lorsque vous avez une réservation en cours.</p><p>Si vous ne trouvez pas le courriel de Noke Pro, écrivez-vous&nbsp;: <a href="mailto:info@locomotion.app">info@locomotion.app</a></p>',
          },
          {
            title: "Comment débarrer le cadenas Noke Pro?",
            content:
              '<div style="position: relative; padding-top: 56.25%; height: 0; width:100%; max-width:560px; margin: 0 auto; margin-bottom: 1rem;"><iframe width="560" height="315" src="https://www.youtube.com/embed/PvyJd2--0sw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="position: absolute; top:0; left: 0; width: 100%; height:100%;"></iframe></div><ol><li>Réservez la remorque sur locomotion.app au moins 15 min avant votre emprunt</li><li>Activez Internet et la fonction Bluetooth sur votre cellulaire</li><li>Si vous avez un système d’exploitation Android, activez la localisation (pas nécessaire avec iPhone)</li><li>Ouvrez l’application Noke Pro<br><em>Important&nbsp;: Ne cliquez sur rien à cette étape.</em><li>Si le cadenas est rond&nbsp;: Appuyez sur l’anneau du cadenas, vers le corps du cadenas, comme si vous vouliez le fermer.<br>Si le cadenas est carré&nbsp;: Pesez sur le bouton en-dessous</li><li>Dans l’application, une icône bleue apparaît, cliquez dessus pour déverrouiller le cadenas.</li></ul><p>Magie! Le cadenas est ouvert :)</p>',
          },
          {
            title: "J’ai un problème avec un cadenas Noke Pro",
            content:
              '<p>Consultez la <a href="https://drive.google.com/file/d/1Zlt8fvoKJyZfxagttq-mZdNcIee9Utf2/view" target="_blank">documentation sur les problèmes fréquents et leurs solutions</a>.</p><p>Le problème persiste? Utilisez notre ligne téléphonique (d’avril à novembre).</p>' +
              '<div class="alert alert-info"><p>Vous avez un problème avec le cadenas?</p><p>Contactez-nous entre 9h et 20h au <a href="tel:438-476-3343" target="_blank" rel="noopener noreferrer">438&nbsp;476-3343</a><br>(cette ligne est dédiée uniquement aux problèmes liés aux cadenas)</p></div>',
          },
          {
            title: "Est-ce qu’il faut activer Internet et le Bluetooth de mon cellulaire?",
            content: "Oui!",
          },
          {
            title: "Si je n’ai pas de téléphone intelligent, qu’est-ce que je fais?",
            content:
              "<p>Nous pourrons vous prêter une puce pour ouvrir les cadenas. En cas de bris ou de perte, elle doit nous être remboursée (30&nbsp;$). Contactez notre équipe, info@locomotion.app!</p>",
          },
        ],
      },
      {
        title: "Emprunter une auto",
        count: "6",
        questions: [
          {
            title: "Qui peut emprunter une auto?",
            content:
              '<p>Il faut avoir au moins 21 ans et avoir complété son dossier de conduite sur locomotion.app (via Mon profil). Deux documents sont nécessaires&nbsp;:<ul><li><a href="https://services.saaq.gouv.qc.ca/FonctionsWeb/EtatDossierConduite.Web/" target="_blank">Dossier de conduite de la SAAQ</a></li><li><a href="https://mondossier.gaa.qc.ca/fr/DemandeWeb/DemandeReleve" target="_blank">Relevé de dossier de sinistres du GAA</a></li></ul><p>Votre dossier sera ensuite approuvé par notre équipe et vous pourrez commencer à faire des demandes d’emprunt pour les autos de vos voisin-e-s!</p>',
          },
          {
            title: "Combien ça coûte?",
            content:
              '<p>Pour que ça soit juste, on contribue&nbsp;:</p><ul><li>aux coûts de possession de l’auto (dépréciation, frais d’intérêt) selon la durée du trajet;</li><li>aux coûts d\'utilisation (entretien, essence, etc.) selon la distance parcourue.</li></ul><p>À ça s’ajoute l’assurance.</p><p>En gros, la formule ressemble à&nbsp;:</p><blockquote style="text-align: center;">Coût d’un emprunt = distance + durée + assurance</blockquote><p>Il y a deux catégories d’auto, qui correspondent à des coûts différents. Pour le détail des coûts, voir la <a href="http://bit.ly/locomotion-tarification" target="_blank">tarification LocoMotion</a>.</p><p>Tous autres frais encourus lors d’un emprunt sont à la charge de l’emprunteur-se (ex. pont payant, stationnement non payé, etc.)</p>',
          },
          {
            title: "Comment payer l’emprunt d’une auto?",
            content:
              "<p>Il faut payer l’emprunt avant de partir avec une auto. Approvisionnez votre compte via le tableau de bord ou faites-le via la page de votre emprunt (après acceptation de la demande d’emprunt de l’auto par son propriétaire).</p><p>Ce prépaiement est une estimation du coût, le montant exact sera pris de votre compte LocoMotion après l’emprunt (kilomètres parcourus réels, achat de carburant, etc.).</p>",
          },
          {
            title: "Partir avec l’auto d’un-e voisin-e",
            content:
              "<p>Une fois la réservation acceptée, c’est la responsabilité de la personne qui emprunte de s’arranger avec le ou la propriétaire sur un lieu et une heure pour récupérer les clés de l’auto.</p>",
          },
          {
            title: "IMPORTANT - Vous avez 48 h pour entrer les infos finales (km, essence)",
            content:
              "<p>Une fois de retour, il est très important de mettre à jour le km parcouru et vos dépenses en essence. Sans ces informations, il sera difficile de donner sa juste part au ou à la propriétaire de l’auto.</p>",
          },
          {
            title: "Modifier les infos d’un emprunt passé  (km départ, km retour, achat carburant)",
            content:
              "<p>Pour l'instant, il n'est pas possible de modifier les informations une fois l’emprunt terminé. Si vous vous rendez compte d’une erreur, dites-le à la personne qui vous a prêté son auto et contactez info@locomotion.app, on vous aidera à ajuster vos factures.</p>",
          },
        ],
      },
      {
        title: "Retard, annulation et frais supplémentaires",
        count: "2",
        questions: [
          {
            title: "Je suis en retard pour le retour d’un véhicule",
            content:
              '<p>Par respect pour les autres, évitons les retards. :-) Mais évidemment, ce genre de choses peut arriver.</p><p style="text-decoration: underline;">S’il s’agit de l’auto d’un-e voisin-e</p><p>Avertissez directement le ou la propriétaire du véhicule. Ensuite, vous pourrez indiquer l’heure réelle de votre retour en cliquant sur le bouton de la page de l’emprunt.</p><p style="text-decoration: underline;">S’il s’agit d’un véhicule collectif</p><p>Dès que possible, allez sur la page de l’emprunt de locomotion.app. Utilisez le bouton &laquo;&nbsp;Signaler un retard&nbsp;&raquo; pour obtenir les coordonnées de la prochaine personne qui a réservé le véhicule. Vous devez la contacter si vous empiétez sur sa réservation. Indiquez votre heure réelle de retour.</p>',
          },
          {
            title: "J'ai annulé ma réservation d’auto, comment puis-je me faire rembourser?",
            content:
              "<p>Si vous annulez votre réservation, l'argent est simplement remis sur votre compte LocoMotion. Vous pouvez retirer cet argent ou l’utiliser sur un prochain trajet. L'info sur l’argent disponible dans votre compte LocoMotion se retrouve sur le tableau de bord (solde).</p><p>Si vous annulez à moins de 24 heures de préavis ou vous ne vous présentez pas pour emprunter l’auto, vous paierez l’assurance.</p>",
          },
        ],
      },
      {
        title: "Prêter mon auto",
        count: "6",
        questions: [
          {
            title: "Combien je reçois lorsque je prête mon auto?",
            content:
              '<p>Consultez le document sur la <a href="http://bit.ly/locomotion-tarification" target="_blank">tarification LocoMotion</a>.</p>',
          },
          {
            title: "Avant de remettre les clés, vérifier le prépaiement",
            content:
              "<p>Avant de remettre les clés de votre auto à votre voisin-e, vérifiez si l’emprunt est prépayé. Si ce n’est pas le cas, rappelez-le gentiment! Ça doit être un simple oubli et cette étape est nécessaire pour la suite :-)</p>",
          },
          {
            title: "Vérifier un prêt d'auto terminé (km, carburant, etc.)",
            content:
              "<p>À la fin d’un prêt d’auto, il est important de vérifier les informations entrées par votre voisin-e dans les 48&nbsp;h. Après ce délai, il ne sera plus possible de contester en cas d’erreur. Pour vérifier, aller sur la page de l’emprunt, via le menu à droite du tableau de bord (&laquo;&nbsp;voir tous les trajets&nbsp;&raquo;) ou via votre profil (&laquo;&nbsp;Historiques des emprunts&nbsp;&raquo;).</p>",
          },
          {
            title: "Annuler un prêt",
            content:
              "<p>Si vous devez annuler le partage de votre auto pour un trajet, contactez rapidement la personne qui pensait pouvoir emprunter votre auto. Elle devra se réorganiser rapidement. Ensuite, annulez l’emprunt sur locomotion.app. La page de l’emprunt est accessible via le tableau de bord, section &laquo;&nbsp;Emprunts en cours&nbsp;&raquo;.</p>",
          },
          {
            title: "Récupérer mon argent",
            content:
              "<p>Une fois le trajet terminé, vous recevrez l’argent du trajet sur votre compte LocoMotion. Dès que vous avez un minimum de 10&nbsp;$ sur votre compte, vous pouvez le récupérer en cliquant sur &laquo;&nbsp;Réclamer&nbsp;&raquo; (bouton sous l’information de votre solde, sur le tableau de bord).</p>",
          },
          {
            title:
              "Quoi faire si je reçois une facture liée à l’emprunt de mon auto par un-e voisin-e?",
            content:
              "<p>Si vous recevez une demande de paiement due à l'utilisation de votre auto par un-e voisin-e (pont payant, contravention, etc.), faites le paiement puis demandez le remboursement à l'emprunteur-se. Pour ce faire, soit vous contactez directement la personne, soit vous demandez à LocoMotion de faire l'intermédiaire en écrivant à <a href=\"mailto:info@locomotion.app\">info@locomotion.app</a>. Le remboursement se fera via un virement interac entre les deux personnes.</p>",
          },
        ],
      },
      {
        title: "Assurances pour les emprunts d’auto",
        count: "4",
        questions: [
          {
            title:
              "Je partage mon auto, est-ce que c’est mon assurance qui est considérée lors d'un accident?",
            content:
              '<p>Non, lorsque vous prêtez votre auto avec LocoMotion, c’est l’assurance Desjardins qui protège votre auto. Vous devez envoyer <a href="https://drive.google.com/file/d/1UPfFkgMRKajgYUtTG9LfpqVqvu75ML_h/view" target="_blank">cette lettre</a> à votre assureur ou courtier d’assurance pour l’aviser que vous participez à LocoMotion (sauf si c’est Desjardins ou La Personnelle, directement). Ça n’a pas d’incidence sur votre contrat.</p>',
          },
          {
            title: "J’emprunte l’auto d’un-e voisin-e, comment suis-je assuré-e?",
            content:
              "<p>Pas besoin de votre assurance personnelle (maison ou auto), dès que vous empruntez une auto avec LocoMotion, c'est notre assurance Desjardins qui couvre votre trajet.</p>",
          },
          {
            title: "Que se passe-t-il en cas de panne?",
            content:
              "<p>L’assurance Desjardins couvre les accidents mais pas les pannes. En cas de panne, le remorquage est aux frais de l’emprunteur-se et la réparation est aux frais du ou de la propriétaire.</p>",
          },
          {
            title: "Quoi faire en cas d’accident ou vandalisme lors d’un emprunt d’auto?",
            content:
              "<p>La procédure à suivre en cas d'accident ou vandalisme lors d’un emprunt d’auto est détaillée dans le document &laquo;&nbsp;<a href=\"https://drive.google.com/file/d/1755OkK35_aph-ol9LpB3BJ2JCnkvkIp1/view\" target=\"_blank\">En cas d'accident</a>&nbsp;&raquo;. Elle est également accessible via le carnet de bord de l'auto et via la page de l'emprunt.</p>",
          },
        ],
      },
      {
        title: "Compte LocoMotion, facturation et paiement",
        count: "6",
        questions: [
          {
            title: "Comment ça marche?",
            content:
              "<p>Les transactions monétaires sur locomotion.app se font par l’intermédiaire de votre compte LocoMotion. Le solde de votre compte est visible en tout temps sur le Tableau de bord.</p><p>Votre solde augmente lorsque&nbsp;:</p><ul><li>Vous approvisionnez votre compte avec votre carte de crédit;</li><li>Vous prêtez votre auto&nbsp;: l’argent de l’emprunteur-se va sur votre compte LocoMotion.</li></ul><p>Votre solde diminue lorsque&nbsp;:</p><ul><li>Vous payez un emprunt (il faut avoir clôturé votre emprunt pour que la transaction se fasse);</li><li>Vous réclamez votre solde. L’argent quitte votre compte LocoMotion et est transféré sur votre compte bancaire.</li></ul>",
          },
          {
            title: "Approvisionner mon compte LocoMotion",
            content:
              "<p>Vous avez besoin d’approvisionner votre compte LocoMotion pour payer les emprunts d’auto et les contributions volontaires. Vous pouvez approvisionner votre compte via le tableau de bord (bouton &laquo;&nbsp;Approvisionner&nbsp;&raquo; sous l’information de votre solde) ou le faire via lors de l’emprunt.</p><p>Des frais de transactions sont prélevés (par Stripe), soit  2,2&nbsp;% + 0,30&nbsp;$. Ainsi, si vous mettez 100&nbsp;$ sur votre compte LocoMotion, c’est 102,50&nbsp;$ qui seront facturés à votre carte de crédit. Vous pouvez réduire les frais de transaction en approvisionnant votre compte avec des montants plus élevés plutôt qu’en payant le montant exact à chaque emprunt.</p>",
          },
          {
            title: "Réclamer l’argent qui est sur mon compte LocoMotion",
            content:
              "<p>Vous pouvez réclamer l’argent qui est sur votre compte en cliquant sur le bouton &laquo;&nbsp;Réclamer&nbsp;&raquo; sur le tableau de bord (sous l’information de votre solde). Un montant minimal de 10&nbsp;$ est requis.</p>",
          },
          {
            title: "Compte bancaire, carte de crédit : quelle différence?",
            content:
              "<p>Sur locomotion.app, la carte de crédit sert à mettre de l’argent sur votre compte LocoMotion et votre compte bancaire sert à récupérer votre argent (si un-e voisin-e vous a emprunté votre auto, par exemple).</p>",
          },
          {
            title: "Comment puis-je faire le suivi de mes paiements?",
            content:
              "<p>Vous trouverez vos factures dans &laquo;&nbsp;Mon profil&nbsp;&raquo;. Vous recevez une facture à chaque fois que votre solde LocoMotion change&nbsp;:</p><ul><li>pour chaque trajet (prêt ou emprunt), ou</li><li>quand vous approvisionnez ou réclamez l’argent de votre compte LocoMotion.</li></ul>",
          },
          {
            title: "Pourquoi des contributions volontaires?",
            content:
              "<p>Pour le moment, les coûts de fonctionnement de la plateforme ne sont pas facturés aux participant-e-s car nous sommes encore en phase pilote. Nous testons notamment comment couvrir ces frais et la contribution volontaire est la piste que nous explorons pour le moment!</p>",
          },
        ],
      },
      {
        title: "Protection de la vie privée",
        count: "1",
        questions: [
          {
            title:
              "Je partage ma photo, mes infos… Est-ce que les gens de mon quartier sont fiables?",
            content:
              '<p>Vos données personnelles, on en prend soin. Voir nos <a href="/conditions">conditions d’utilisation</a>.</p>',
          },
        ],
      },
      {
        title: "Les territoires LocoMotion",
        count: "5",
        questions: [
          {
            title: "Pourquoi LocoMotion existe seulement dans certains quartiers?",
            content:
              "<p>LocoMotion n’est pas un service clé en main, c’est un projet citoyen en cours de développement. On le construit ensemble en se concentrant dans certains quartiers avant de l’adapter à d’autres réalités.</p>",
          },
          {
            title: "Voisinage, quartier : quelle différence?",
            content:
              '<p class="text-center"><img src="/schema-quartiers.png"></p>' +
              "<p>Le voisinage est dans le quartier, le quartier est dans l’arrondissement… &laquo;&nbsp;L’arbre est dans ses feuilles, Marilon Marilé!&nbsp;&raquo;</p><p>Être proche de ses voisin-e-s, c’est la base de LocoMotion. C’est pour cette raison qu’on privilégie la création de sous-groupes LocoMotion à l’échelle très très locale.</p><p>Un voisinage LocoMotion est créé par des gens qui habitent à 5-10 minutes à pied. Ça prend des personnes qui veulent s’impliquer dans le projet et le faire évoluer.</p><p>Comment favoriser les liens dans le voisinage? Est-ce qu’on veut ajouter à notre flotte collective un vélo électrique ou des remorques? Solon vous accompagne!</p><p>Le quartier est plus grand. Il réfère à un territoire urbain reconnu par la population et les institutions locales (par exemple, La Petite-Patrie) et peut contenir plusieurs voisinages. Participer à LocoMotion dans son quartier, c'est avoir accès à certains véhicules et c'est une porte d'entrée vers la création de son voisinage!</p><p>Les véhicules sont pour le voisinage ou le quartier? L'ensemble des participant-e-s du quartier peut profiter des véhicules collectifs (appartiennent à Solon). Les propriétaires d'un véhicule (auto, vélo-cargo ou autre) peuvent décider de le rendre disponible à leur voisinage ou au quartier au complet.</p>",
          },
          {
            title: "Est-ce que LocoMotion est disponible là où j'habite?",
            content:
              '<p>LocoMotion est accessible seulement aux <a href="https://locomotion.app/communities" target="_blank">voisinages et quartiers visibles sur cette carte</a>. Si vous résidez dans un quartier où LocoMotion est disponible, mais qu’il n’y a pas encore de voisinage autour de chez vous, vous pourriez en créer un avec vos voisin-e-s! En savoir plus sur <a href="https://mailchi.mp/solon-collectif.org/comment-creer-un-voisinage" target="_blank">comment créer un voisinage</a>.</p>',
          },
          {
            title: "Je partage mon véhicule au voisinage ou au quartier?",
            content:
              '<p>Par défaut, on met à disposition votre auto, votre vélo ou autre véhicule à très petite échelle, à votre voisinage. Ensuite, vous avez le choix. Vous pouvez <a href="/profile/loanables">changer vos préférences de partage</a> pour en faire profiter tout le quartier!</p>',
          },
          {
            title:
              "On est une gang motivée de voisines et de voisins, comment partir un voisinage LocoMotion?",
            content:
              '<p>La mise en œuvre de LocoMotion dans de nouveaux voisinages demande du temps, des personnes motivées et de la persévérance! Pour savoir si vous résidez dans un quartier où c’est possible et en savoir plus sur comment faire, rendez-vous sur <a href="https://mailchi.mp/solon-collectif.org/comment-creer-un-voisinage" target="_blank">comment créer un voisinage</a>.</p>',
          },
        ],
      },
      {
        title: "Envie d’en faire plus? Rejoignez votre comité de voisinage!",
        count: "1",
        questions: [
          {
            title: "Le comité de voisinage LocoMotion, qu’est-ce que ça implique?",
            content:
              "<p>Pour que LocoMotion vive dans votre voisinage, en plus des gens qui se partagent autos et vélos, il faut également un noyau de personnes plus impliquées pour remplir les rôles suivants&nbsp;:</p>" +
              "<dl>" +
              "<dt>MENTOR 👋</dt>" +
              "<dd>Accueille et explique.</dd>" +
              "<dd>Aime expliquer et répondre aux questions.</dd>" +
              "<dt>PORTE-PAROLE 📣</dt>" +
              "<dd>Fait rayonner.</dd>" +
              "<dd>Veut faire connaître LocoMotion dans le voisinage.</dd>" +
              "<dt>MAESTRO 🎶</dt>" +
              "<dd>Anime son voisinage.</dd>" +
              "<dd>Aime organiser et animer des événements.</dd>" +
              "<dt>MÉCANO 🔧</dt>" +
              "<dd>Veille sur les véhicules collectifs.</dd>" +
              "<dd>Aime la mécanique de vélo et les tâches manuelles.</dd>" +
              "</dl>" +
              "<p>Pour rejoindre votre comité, contactez-les comme indiqué à la page Voisinage de locomotion.app.</p><p>Le comité fait régulièrement appel à tou-te-s participant-e-s pour contribuer de différentes façons. Par exemple pour construire des abris en bois pour les remorques ou encore pour contribuer à des ateliers de réflexion sur le projet et à des prises de décision collectives.</p>",
          },
        ],
      },
    ],
  },
};
